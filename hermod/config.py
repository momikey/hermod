# dotenv-based configuration class
# This is for application-level configuration, the kind of thing that can't be adjusted on the fly.

from typing import Optional
from pydantic_settings import BaseSettings

class AppConfiguration(BaseSettings):
    """Application configuration class. This attempts to read from a .env file and/or environment variables.
    
    Only those configuration settings that can't be adjusted while the application is running (such as DB credentials) will go in here. "User-level" settings do not.
    """

    # Development or production mode.
    mode = "development"

    # A secret key used for token signing.
    secret_key = "none"


    # Database info

    db_connection = "postgresql"
    db_dialect : Optional[str] = None
    db_host : Optional[str] = "localhost"
    db_port : Optional[str] = "5432"
    db_username : str
    db_password : str
    db_name : Optional[str] = "hermod"

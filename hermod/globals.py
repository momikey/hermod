# Application-level variables that are constant through the lifetime of the server.

from hermod.config import AppConfiguration

# An instance of the application configuration, loaded from .env or environment variables.
app_configuration = AppConfiguration()

import urllib.parse

from sqlalchemy import create_engine
from sqlalchemy.engine.base import Connection, Engine
from sqlalchemy.orm import declarative_base, sessionmaker

from hermod.config import AppConfiguration

settings = AppConfiguration()

# Merge the DB type and dialect if the user gave the dialect.
# We will support multiple databases and drivers, so this is necessary.
db_and_dialect = settings.db_connection
if settings.db_dialect is not None:
    db_and_dialect += f"+{settings.db_dialect}"

# SQLAlchemy wants passwords to be URL-encoded. Ugh.
db_password = urllib.parse.quote_plus(settings.db_password)

SQLALCHEMY_DATABASE_URL = (
    f"{db_and_dialect}://{settings.db_username}:{db_password}@{settings.db_host}:{settings.db_port}/{settings.db_name}"
)

# Create all the engine and ORM stuff.
engine = create_engine(SQLALCHEMY_DATABASE_URL, pool_pre_ping=True, pool_size=10, max_overflow=20)
Session = sessionmaker(autoflush=False, bind=engine)
TableBase = declarative_base()

def Database():
    """FastAPI database dependency for injection"""

    db = Session()
    try:
        yield db
    finally:
        db.close()
